"use strict";

const Command = require("@puzzleframework/cli/Extend/Command");
const MigrationRunner = require("../Runner/MigrationRunner");

class Migration extends Command {
  constructor() {
    super("db:migrate <action>", "Database migration tool");
  }

  builder(yargs) {
    return yargs.options({
      module: {
        description: "The module for which we run the migrations",
        type: "string",
        default: ""
      },
      folder: {
        description: "The migrations folder",
        type: "string",
        default: "Migrations"
      }
    })
      .positional("action", {
        description: "The desired action to do while executing a migration",
        type: "string",
        choices: ["up", "down", "reset", "status"]
      });
  }

  async run(argv) {
    const migrationRunner = new MigrationRunner(this.log);
    migrationRunner.disableFileLogging();
    await migrationRunner.run(argv.action, argv.module, argv.folder);
  }
}

module.exports = Migration;
