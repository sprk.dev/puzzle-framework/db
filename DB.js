"use strict";

const sequelize = require("sequelize");

/**
 * Module DB
 *
 * TODO: Update this documentation
 */
module.exports = {
  Sequelize: sequelize,
  DataTypes: sequelize.DataTypes,
  Model: sequelize.Model
};
