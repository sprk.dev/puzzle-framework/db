# Database Module for Puzzle Framework

This module provides Database and ORM functionality to the Puzzle Framework.

To use this module, add the "@puzzleframework/db" in your application "package.json"
under the "puzzles" property as up as possible in the list. 

## Contributing

To contribute please read the [CONTRIBUTING.md](https://gitlab.com/sprk.dev/puzzle-framework/core/-/blob/master/CONTRIBUTING.md) file.
