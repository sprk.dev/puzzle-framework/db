"use strict";

const CLI = require("@puzzleframework/core/CLI/CLI");
const PRuntime = require("@puzzleframework/core/Core/PRuntime");
const puzzle = require("@puzzleframework/core/Puzzle");

const Migration = require("./Command/Migration");
const DatabaseConnection = require("./Util/DatabaseConnection");

class DatabaseRuntime extends PRuntime {
  /**
   * @return {Sequelize}
   */
  get default() {
    return DatabaseConnection.getInstance().default;
  }

  use(engine) {
    engine.set("db", this);
  }

  beforeBoot() {
    if (!puzzle.http) {
      CLI.Loader.register(new Migration());
    }
  }
}

module.exports = DatabaseRuntime;
