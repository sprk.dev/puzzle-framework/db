"use strict";

const fs = require("fs");
const path = require("path");

/**
 * Finds the right path for the given module.
 *
 * @param {string} module The module name.
 * @param {string} folder The folder name.
 *
 * @return {string}
 */
function FindPathUtil(module, folder = "") {
  const pathList = [];
  pathList.push(path.join(process.cwd(), "puzzles", module, folder));
  pathList.push(path.join(process.cwd(), "puzzles", module.replace(new RegExp(/\./, "gi"), "/"), folder));
  pathList.push(path.join(process.cwd(), "src", "puzzles", module, folder));
  pathList.push(path.join(process.cwd(), "src", "puzzles", module.replace(new RegExp(/\./, "gi"), "/"), folder));
  pathList.push(path.join(process.cwd(), "dist", "puzzles", module, folder));
  pathList.push(path.join(process.cwd(), "dist", "puzzles", module.replace(new RegExp(/\./, "gi"), "/"), folder));
  pathList.push(path.join(process.cwd(), "node_modules", module, folder));
  pathList.push(path.join(process.cwd(), "node_modules", module.replace(new RegExp(/\./, "gi"), "/"), folder));
  pathList.push(path.join(process.cwd(), "node_modules", module, "src", folder));
  pathList.push(path.join(process.cwd(), "node_modules", module.replace(new RegExp(/\./, "gi"), "/"), "src", folder));
  pathList.push(path.join(process.cwd(), "node_modules", module, "dist", folder));
  pathList.push(path.join(process.cwd(), "node_modules", module.replace(new RegExp(/\./, "gi"), "/"), "dist", folder));

  const thePath = pathList.find((basePath) => fs.existsSync(basePath));

  return thePath || "";
}

module.exports = FindPathUtil;
