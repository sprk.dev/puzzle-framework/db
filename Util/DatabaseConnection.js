"use strict";

const moment = require("moment-timezone");
const PObject = require("@puzzleframework/core/Core/PObject");
const puzzle = require("@puzzleframework/core/Puzzle");

const ConfigurationNotFoundException = require("@puzzleframework/core/Exceptions/ConfigurationNotFoundException");

const { Sequelize } = require("../DB");

/**
 * Class DatabaseConnection
 */
class DatabaseConnection extends PObject {
  /**
   *
   * @protected
   *
   * @property {Map<string, Sequelize>}
   */
  static _connections = [];

  /**
   *
   * @protected
   *
   * @property {DatabaseConnection|null}
   */
  static _instance = null;

  /**
   *
   * @return {Sequelize}
   */
  get default() {
    return this.connect(puzzle.env);
  }

  static getInstance() {
    if (!DatabaseConnection._instance) {
      DatabaseConnection._instance = new DatabaseConnection();
    }
    return DatabaseConnection._instance;
  }

  static getConnection(environment = "dev") {
    return DatabaseConnection.connect(environment);
  }

  connect(environment = "dev") {
    if (this.isValid(DatabaseConnection._connections[environment])) {
      return DatabaseConnection._connections[environment];
    }

    if (!this.isValid(puzzle.config.db) || !this.isValid(puzzle.config.db[environment])) {
      throw new ConfigurationNotFoundException("config.db", `config.db.${environment}`);
    }

    const dbConfig = puzzle.config.db[environment];

    const sequelizeOptions = {
      database: dbConfig.database,
      username: dbConfig.user,
      password: dbConfig.password,
      host: dbConfig.host,
      port: dbConfig.port,
      dialect: dbConfig.driver,
      logging: (message) => {
        puzzle.log.debug(message);
      }
    };

    if (dbConfig.driver === "mysql") {
      sequelizeOptions.timezone = "local";
      if (this.isValid(dbConfig.timezone)) {
        sequelizeOptions.timezone = moment()
          .tz(dbConfig.timezone)
          .format("Z");
      }

      puzzle.log.debug(`Selected timezone: ${sequelizeOptions.timezone}${dbConfig.timezone ? ` [FROM ${dbConfig.timezone}]` : ""}`);
    }

    DatabaseConnection._connections[environment] = new Sequelize(sequelizeOptions);

    return DatabaseConnection._connections[environment];
  }
}

module.exports = DatabaseConnection;
