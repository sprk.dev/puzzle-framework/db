"use strict";

const PModel = require("../PModel");
const { DataTypes, Sequelize } = require("../DB");
const DatabaseConnection = require("./DatabaseConnection");

class MigrationModel extends PModel {
}

MigrationModel.init({
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  },
  module: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: ""
  },
  runDate: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: Sequelize.Sequelize.fn("NOW")
  }
}, {
  sequelize: DatabaseConnection.getInstance().default,
  tableName: "_migrations",
  timestamps: false,
  charset: "utf8",
  collate: "utf8_unicode_ci",
});

module.exports = MigrationModel;
