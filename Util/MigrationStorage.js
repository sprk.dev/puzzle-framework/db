"use strict";

const Storage = require("umzug/lib/storages/Storage").default;

const MigrationModel = require("./MigrationModel");

class MigrationStorage extends Storage {
  #module = "";

  constructor(module) {
    super();

    this.#module = module || "";
  }

  /**
   * Logs migration to be considered as executed.
   *
   * @param {string} migrationName Name of the migration to be logged.
   *
   * @return {Promise<MigrationModel>}
   */
  async logMigration(migrationName) {
    await MigrationModel.sync();

    return MigrationModel.create({
      name: migrationName,
      module: this.#module
    });
  }

  /**
   * Unlogs migration to be considered as pending.
   *
   * @param {string} migrationName - Name of the migration to be unlogged.
   *
   * @return {Promise<MigrationModel>}
   */
  async unlogMigration(migrationName) {
    await MigrationModel.sync();

    return MigrationModel.destroy({
      where: {
        name: migrationName,
        module: this.#module,
      }
    });
  }

  /**
   * Gets list of executed migrations.
   *
   * @return {Promise<Array<string>>}
   */
  async executed() {
    await MigrationModel.sync();

    const migrations = await MigrationModel.findAll({
      where: {
        module: this.#module,
      }
    });

    return migrations.map((migration) => migration.name);
  }
}

module.exports = MigrationStorage;
