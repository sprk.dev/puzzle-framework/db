"use strict";

const PRunner = require("@puzzleframework/core/TaskRunner/PRunner");
const umzug = require("umzug");
const path = require("path");
const puzzle = require("@puzzleframework/core/Puzzle");
const MigrationStorage = require("../Util/MigrationStorage");
const { DataTypes } = require("../DB");
const FindPathUtil = require("../Util/FindPathUtil");

class MigrationRunner extends PRunner {
  /**
   *
   * @protected
   *
   * @property {_umzug}
   */
  _umzug = null;

  /**
   *
   * @private
   *
   * @property {string[]}
   */
  #availableActions = [
    "up", "down", "reset", "status"
  ];

  /**
   *
   * @private
   *
   * @param {string} whatToDo
   *
   * @return {string}
   */
  _transformAction(whatToDo) {
    return `_migrate${whatToDo.substring(0, 1).toUpperCase()}${whatToDo.substring(1)}`;
  }

  /**
   * Task runner entry point.
   *
   * @async
   *
   * @param {string} whatToRun What has to be ran.
   * @param {string} module For which module we run the migrations.
   * @param {string} folder In which folder we look for the migrations.
   */
  async run(whatToRun, module, folder) {
    if (this.#availableActions.indexOf(whatToRun) < 0) {
      this.log.error(`Unable to find command ${whatToRun}`);
      return;
    }

    folder = this.isValid(folder) ? folder : "Migrations";

    if (module !== "") {
      this._initUmzug(module, FindPathUtil(module, folder));
    } else {
      this._initUmzug();
    }

    try {
      await this[this._transformAction(whatToRun)]();
      this.log.info(`${whatToRun.toUpperCase()} DONE`);
    } catch (err) {
      this.log.error(`${whatToRun.toUpperCase()} ERROR`);
      if (err !== null && err !== undefined) {
        this.log.error(err);
      }
      throw err;
    }
  }

  /**
   * Initialization of the Umzug migration engine.
   *
   * @protected
   *
   * @param {string} [module=""] The module for which we run the migrations.
   * @param {string} [migrationPath=""] The path to migrations that we want to run.
   */
  _initUmzug(module = "", migrationPath = "") {
    if (!this.isValid(migrationPath) || migrationPath === "") {
      migrationPath = puzzle.config.db.migrationsPath;
      module = "";
    }

    this._umzug = new umzug({
      storage: new MigrationStorage(module),
      migrations: {
        params: [
          puzzle.db.default.getQueryInterface(), // queryInterface
          DataTypes, // DataTypes
        ],
        path: path.resolve(migrationPath),
        pattern: /\.js$/
      },

      logging: (...args) => {
        this.log.debug(...args);
      },
    });
    this._umzug.on("migrating", this._umzugEvent("migrating"));
    this._umzug.on("migrated", this._umzugEvent("migrated"));
    this._umzug.on("reverting", this._umzugEvent("reverting"));
    this._umzug.on("reverted", this._umzugEvent("reverted"));
  }

  /**
   * Displays the status of the current run on the console and in the log.
   *
   * @protected
   *
   * @param {string} eventName The name of the event.
   *
   * @return {Function}
   */
  _umzugEvent(eventName) {
    /**
     * Callback that is used when a migration event is triggered. Logs the status of the
     * event.
     *
     * @param {string} name The name of the current running migration.
     * @param {*} migration All the information available for the current migration.
     */
    return (name, migration) => {
      this.log.info(`${eventName.toUpperCase()}: ${name}`);
    };
  }

  /**
   * Undos the last migration that was run.
   *
   * Command to run: `db:migrate down`
   *
   * @protected
   *
   * @return {Promise}
   */
  _migrateDown() {
    return this._umzug.down();
  }

  /**
   * Undos all migrations that were run.
   *
   * Command to run: `db:migrate reset`
   *
   * @protected
   *
   * @return {Promise}
   */
  _migrateReset() {
    return this._umzug.down({
      to: 0
    });
  }

  /**
   * Runs all the migrations that weren't loaded into the application from
   * the migrations folder.
   *
   * Command to run: `db:migrate up`
   *
   * @protected
   *
   * @return {Promise}
   */
  _migrateUp() {
    return this._umzug.up();
  }

  /**
   * Displays the status of the migrations (which migrations were run, which aren't run,
   * which is the latest migration that was run).
   *
   * Command to run: `db:migrate status`
   *
   * @protected
   *
   * @return Promise<object>
   */
  async _migrateStatus() {
    let executed = await this._umzug.executed();
    let pending = await this._umzug.pending();

    executed = executed.map((exec) => {
      exec.name = path.basename(exec.file, ".js");
      return exec;
    });
    pending = pending.map((pend) => {
      pend.name = path.basename(pend.file, ".js");
      return pend;
    });

    const current = executed.length > 0 ? executed[0].file : "<NO_MIGRATIONS>";

    this.log.info("Current migration to be run:");
    this.log.info(current);
    this.log.info("Migrations that have ran:");
    this.log.info(executed.map((exec) => `\t- ${exec.file}`).join("\n").trim() || "\t- NONE -");
    this.log.info("Pending migrations:");
    this.log.info(pending.map((pend) => `\t- ${pend.file}`).join("\n").trim() || "\t- NONE -");

    return {
      executed,
      pending
    };
  }
}

module.exports = MigrationRunner;
